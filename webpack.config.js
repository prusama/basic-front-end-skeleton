const path = require('path');

module.exports = {
    mode: "production",
    entry: path.join(__dirname, 'src', 'js', 'index.ts'),
    output: {
        filename: 'dist/app.js',
        path: __dirname
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
            }
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    }
};
